import React, { Component } from "react";

/*

По клику на компонент Button должен
показываться компонент Alert,
он должен просто показать текст, который
приходит в пропс message и с цветом пропса color

*/

function Alert(props) {
    return (
        <p className="alert" style = {{color: props.color}}>
            {props.message}
        </p>
    )
}

function MyButton(props) {
    return (
        <div className="btn">
            <button onClick = {props.onClick}>Кнопка</button>
        </div>
    )
}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: false
        }
        this.showMessage = this.showMessage.bind(this)
    }

    showMessage() {
        const {message} = this.state;
        this.setState({message: !message})
    }
    render() {
        return (
        <div className="App">
            <MyButton onClick = {this.showMessage}/>
            {this.state.message === true ? <Alert message="Hi!" color="red" /> : ""}
        </div>
        );
    }
}

export default App;
